package sk.murin.automat;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Automat extends JFrame {
    private final Font f = new Font("Courier", Font.BOLD, 35);
    private JLabel labelInsertedMoney, labelRate;
    private final JLabel[] labels = new JLabel[9];
    private final ImageIcon[] poleIcon = {new ImageIcon("tentobanan.png"), new
            ImageIcon("tentoorange.png"), new ImageIcon("tentoraspberry.png")};
    private double insertedMoneyDouble, insertedRateDouble;
    private final Random r = new Random();
    private JButton btnPlay;

    public Automat() {
        setup();

    }

    private void setup() {
        setTitle("Hraj sa");
        setSize(550, 550);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());
        add(makePanelCenter(), BorderLayout.CENTER);
        add(makePanelBottom(), BorderLayout.SOUTH);
        add(makePanelTop(), BorderLayout.NORTH);
        setVisible(true);
        for (int i = 0; i < poleIcon.length; i++) {
            poleIcon[i] = getScaledImage(poleIcon[i], 100, 100);
        }
        insertMoney();
    }

    private JPanel makePanelTop() {
        JPanel panel = new JPanel();
        GridLayout gridLayout = new GridLayout(1, 2);

        panel.setLayout(gridLayout);
        labelInsertedMoney = new JLabel("Your current money balance is :");
        labelInsertedMoney.setFont(new Font("Courier", Font.BOLD, 15));
        labelRate = new JLabel("Your bet rate is :");
        labelRate.setFont(new Font("Courier", Font.BOLD, 15));
        labelInsertedMoney.setHorizontalAlignment(SwingConstants.CENTER);
        labelRate.setHorizontalAlignment(SwingConstants.LEADING);
        panel.add(labelInsertedMoney);
        panel.add(labelRate);
        return panel;
    }

    private JPanel makePanelCenter() {

        JPanel panelCenter = new JPanel();
        panelCenter.setLayout(new GridLayout(3, 3));
        //panelCenter.setPreferredSize(new Dimension(550,));
        for (int i = 0; i < 9; i++) {
            labels[i] = new JLabel();
            labels[i].setHorizontalAlignment(SwingConstants.CENTER);
            labels[i].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            labels[i].setFont(f);
            labels[i].setName("");
            panelCenter.add(labels[i]);
        }

        return panelCenter;
    }

    private JPanel makePanelBottom() {
        JPanel panel = new JPanel();
        btnPlay = new JButton("Play");
        btnPlay.setFocusable(false);
        btnPlay.addActionListener(e -> {
            generateIcon();
            check();
        });
        panel.add(btnPlay);
        return panel;
    }

    private static ImageIcon getScaledImage(ImageIcon srcImg, int w, int h) {
        Image image = srcImg.getImage();
        Image newimg = image.getScaledInstance(w, h, java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
        return new ImageIcon(newimg);
    }

    private void insertMoney() {
        while (true) {
            try {
                String insertedMoney = JOptionPane.showInputDialog(null, "How much € do you want to insert?(Minimum deposit is 10€)", "How much??", JOptionPane.QUESTION_MESSAGE);
                insertedMoneyDouble = Double.parseDouble(insertedMoney);
                if (insertedMoneyDouble > 9) {
                    labelInsertedMoney.setText("Your money balance is " + insertedMoneyDouble + " €");
                    insertRate();
                    break;
                } else {
                    JOptionPane.showMessageDialog(null, "Your deposit must be higher than 10", "Money > 10", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Wrong input", "ERROR!!", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    private void insertRate() {
        while (true) {
            try {
                String insertedRate = JOptionPane.showInputDialog(null, "How many € will be your bet?", "How much??", JOptionPane.QUESTION_MESSAGE);
                insertedRateDouble = Integer.parseInt(insertedRate);
                if (insertedMoneyDouble > insertedRateDouble) {
                    labelRate.setText("Your bet rate is " + insertedRateDouble + " €");
                    break;
                } else {
                    JOptionPane.showMessageDialog(null, "Your deposit must be higher than your bet rate", "Money > rate", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception exc) {
                JOptionPane.showMessageDialog(null, "Wrong input", "ERROR222222!!", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    private void check() {
        double zaloha = insertedMoneyDouble;
        if (checkRows()) {
            insertedMoneyDouble += (insertedRateDouble * 5);
            System.out.println("pripocitalo sa " + (insertedMoneyDouble - zaloha));
        } else {
            insertedMoneyDouble -= insertedRateDouble;
            labelInsertedMoney.setText(String.format("Your money balance is %.2f €", insertedMoneyDouble));
            if (insertedMoneyDouble <= 0) {
                JOptionPane.showMessageDialog(null, "You lose everything HAHAHAHAH", "END!!", JOptionPane.INFORMATION_MESSAGE);
                btnPlay.setEnabled(false);
            }
        }
    }

    public void generateIcon() {
        int r = this.r.nextInt(poleIcon.length);
        int randPosition = this.r.nextInt(labels.length);

        if (String.valueOf(r).equals(labels[randPosition].getName())) {
            r++;
            r = r % poleIcon.length;        //aby sa negeneroval ta ista

        }
        labels[randPosition].setName(String.valueOf(r));
        labels[randPosition].setIcon(poleIcon[r]);
    }

    private boolean checkRows() {
        if (!labels[0].getName().equals("") && labels[0].getName().equals(labels[1].getName())
                && labels[1].getName().equals(labels[2].getName())) {
            return true;
        }
        if (!labels[3].getName().equals("") && labels[3].getName().equals(labels[4].getName())
                && labels[4].getName().equals(labels[5].getName())) {
            return true;
        }
        if (!labels[6].getName().equals("") && labels[6].getName().equals(labels[7].getName())
                && labels[7].getName().equals(labels[8].getName())) {
            return true;
        }
        return false;
    }
}


